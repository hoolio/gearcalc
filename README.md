# Calculate bicycle gear ratios etc in python

assuming some data 

```
trek_data = {
  "name": "Trek 1000",
  "chainrings": [52,42,32],
  "cassette": [11, 12, 13, 14, 15, 17, 19, 21, 23, 25],
  "crank_length_mm": 170,
  "wheel_size_mm": 622,
  "tyre_size_mm": 28
}
trek = Bike(trek_data)
```
running
```
print(trek.dataframe['gear_inches'].max())
```
would return
```
126.18468146027203
```

code uses panda dataframes and the data resides in the .dataframe attribute

## how to use it
start a jupyter server and play with the ipynb.

..ymmv.. ;)

## references

Mainly the venerable Sheldon Brown.