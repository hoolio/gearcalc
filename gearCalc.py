import pandas as pd
import numpy as np

class Bike:
    def __init__(self, bike_dict):
        self.name = bike_dict['name']
        self.short_name = bike_dict['short_name']
        self.chainrings = bike_dict['chainrings']
        self.cassette = bike_dict['cassette']
        self.crank_length_mm = bike_dict['crank_length_mm']
        self.wheel_size_mm = bike_dict['wheel_size_mm']
        self.tyre_size_mm = bike_dict['tyre_size_mm']

        self.fd_model = bike_dict['fd_model']
        self.fd_teeth_capacity = bike_dict['fd_teeth_capacity']
        self.rd_model = bike_dict['rd_model']
        self.rd_wrap_capacity = bike_dict['rd_wrap_capacity']

        self.cassette_diff = [self.cassette[i+1] - self.cassette[i] for i in range(len(self.cassette)-1)]
        self.cassette_diff_pct = self.calc_cassette_diff_pct()

        self.wheel_diameter_mm = self.calculate_wheel_diameter_mm(self.wheel_size_mm, self.tyre_size_mm)

        self.dataframe = self.calculate_dataframe(self.chainrings, self.cassette, self.wheel_diameter_mm, self.crank_length_mm)

        self.chainring_count = len(self.chainrings)
        self.cog_count = len(self.cassette)
        self.gears = self.chainring_count * self.cog_count

        self.required_rd_chain_wrap = self.calc_required_rd_chain_wrap()
        self.required_fd_capacity = self.calc_required_fd_capacity()

    def __repr__(self):
        return f"The {self.name} runs {self.gears} gears in a {self.chainring_count} by {self.cog_count} config"
        #return f"{display(self.gear_inch_df)}"
    
    @staticmethod
    def calculate_wheel_diameter_mm(wheel_size_mm, tyre_size_mm):
        return (tyre_size_mm * 2) + wheel_size_mm
    
    @staticmethod
    def convert_mm_to_inches(mm):
        return mm / 25.4
    
    def calc_cassette_diff_pct(self):
        s = pd.Series(self.cassette)
        cassette_diff_pct = s.pct_change()

        return cassette_diff_pct
    
    def calc_required_rd_chain_wrap(self):
        min_chainring = min(self.chainrings)
        max_chainring = max(self.chainrings)
        min_cog = min(self.cassette)
        max_cog = max(self.cassette)   

        required_rd_chain_wrap = (max_chainring - min_chainring) + (max_cog - min_cog)

        return required_rd_chain_wrap   
    
    def calc_required_fd_capacity(self):
        min_chainring = min(self.chainrings)
        max_chainring = max(self.chainrings)
   
        required_fd_capacity = max_chainring - min_chainring

        return required_fd_capacity   
    
    def calc_gain_ratio(self, chainring, cog, wheel_diameter_mm, crank_length_mm):
        # https://www.sheldonbrown.com/gain.html

        radius_ratio = (wheel_diameter_mm / 2) / crank_length_mm

        gain_ratio = (radius_ratio * chainring) / cog

        return gain_ratio
    
    def calc_gear_inches(self, chainring, cog, wheel_diameter_mm, verbose=False):
        # Sheldon says it is very easy to calculate gear inches: 
        # the diameter of the drive wheel, times the size of the front sprocket divided by the size of the rear sprocket.

        wheel_diameter_inches = Bike.convert_mm_to_inches(wheel_diameter_mm)

        gear_inches = (wheel_diameter_inches * chainring) / cog

        if verbose:
            print("calculating for a {} {}:".format(chainring, cog))
            print(".. gear_inches = {}".format(round(gear_inches, 2)))
        
        return gear_inches

    # uses a pretty df and simple calculations to produce a long form df. 
    def calculate_dataframe(self, chainrings, cassette, wheel_diameter_mm, crank_length_mm):
        from itertools import product

        combinations = list(product(chainrings, cassette))

        df = pd.DataFrame(combinations, columns=['chainring', 'cassette'])

        df['gear_inches'] = self.calc_gear_inches(df['chainring'], df['cassette'], wheel_diameter_mm)
        df['gain_ratio'] = self.calc_gain_ratio(df['chainring'], df['cassette'], wheel_diameter_mm, crank_length_mm)
        df['gear_ratio'] = df['chainring'] / df['cassette']
        #df['percent_diff'] = df['gear_inches'].pct_change() * 100

        df.attrs['metadata'] = {'name': self.name}

        return(df)
    
    def show_gear_inches(self):
        table = self.dataframe.pivot(index='cassette', columns='chainring', values='gear_inches')

        sorted_table = table.sort_index(axis=1, ascending=False)

        display(sorted_table)

    def show_gain_ratio(self):
        table = self.dataframe.pivot(index='cassette', columns='chainring', values='gain_ratio')

        sorted_table = table.sort_index(axis=1, ascending=False)

        display(sorted_table)

    def show_gear_ratio(self):
        table = self.dataframe.pivot(index='cassette', columns='chainring', values='gear_ratio')

        sorted_table = table.sort_index(axis=1, ascending=False)

        display(sorted_table)