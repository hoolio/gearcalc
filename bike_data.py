specialised_data = {
  "name": "Specialised Diverge Comp E5",
  "short_name": "diverge",
  "chainrings": [48, 32],  
  "cassette": [11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 32],
  "crank_length_mm": 170,
  "wheel_size_mm": 622,
  "tyre_size_mm": 40,   
  "fd_model":"UNKNOWN",
  "fd_teeth_capacity":9999,
  "rd_model":"UNKNOWN",
  "rd_wrap_capacity":9999
}

nicasso_actual = {
  "name": "Marin Nicasso",
  "short_name": "nicasso",
    "chainrings": [50, 39, 28], # rvbc ta rings
  "cassette": [11, 12, 14, 16, 18, 21, 24, 28, 32],
  "crank_length_mm": 175,
  "wheel_size_mm": 622,
  "tyre_size_mm": 40,   
  "fd_model":"Ultegra Triple FD6503 < UNCONFIRMED!",
  "fd_teeth_capacity":22,
  "rd_model":"RD-M772 < UNCONFIRMED!",
  "rd_wrap_capacity":44
}

nicasso_granny = {
  "name": "Marin Nicasso",
  "short_name": "nicasso",
  #"chainrings": [52, 42, 30], # option a, original
  #"chainrings": [50, 39, 28], # option b, rvbc ta rings
  "chainrings": [48, 38, 26], # fictional  
  #"cassette": [11, 12, 14, 16, 18, 21, 24, 28, 32], # installed
  "cassette": [11, 13, 15, 17, 20, 23, 26, 30, 34], # fictional
  "crank_length_mm": 175,
  "wheel_size_mm": 622,
  "tyre_size_mm": 40,   
  "fd_model":"Ultegra Triple FD6503 < UNCONFIRMED!",
  "fd_teeth_capacity":22,
  "rd_model":"RD-M772 < UNCONFIRMED!",
  "rd_wrap_capacity":44
}

# old bikes
trek_1000_data = {
  "name": "Hoolios Trek 1000",
  "short_name": "trek_1000",
  "chainrings": [52,42,32],
  "cassette": [11, 12, 13, 14, 15, 17, 19, 21, 23, 25],
  "crank_length_mm": 170,
  "wheel_size_mm": 622,
  "tyre_size_mm": 28,
  "fd_model": "Ultegra Triple FD6503 < UNCONFIRMED!",
  "fd_teeth_capacity":22
}

shogun_data = {
  "name": "Hoolios Shogun Metro SE",
  "short_name": "shogun",
  "chainrings": [42,32,22],
  "cassette": [11, 13, 15, 18, 21, 24, 28],
  "crank_length_mm": 170,
  "wheel_size_mm": 622,
  "tyre_size_mm": 40,
  "fd_model": "UNKNOWN",
  "fd_teeth_capacity":9999,
  "rd_model":"UNKNOWN",
  "rd_wrap_capacity":9999
}

# other peoples bikes
ridley_data = {
  "name": "Kevs Ridley X-Trail",
  "short_name": "ridley",
  "chainrings": [48, 32],
  "cassette": [11, 12, 13, 14, 16, 18, 20, 22, 25, 28, 32],
  "crank_length_mm": 170,
  "wheel_size_mm": 622,
  "tyre_size_mm": 40,   
  "fd_model":"UNKNOWN",
  "fd_teeth_capacity":9999,
  "rd_model":"UNKNOWN",
  "rd_wrap_capacity":9999
}

trek_520_data = {
  "name": "Hobbits Trek 520",
  "short_name": "trek_520",
  "chainrings": [52,42,30],
  "cassette": [11, 13, 15, 17, 20, 23, 26, 30, 34],  
  "crank_length_mm": 170,
  "wheel_size_mm": 622,
  "tyre_size_mm": 32,
  "fd_model": "UNKNOWN",
  "fd_teeth_capacity":9999,
  "rd_model":"UNKNOWN",
  "rd_wrap_capacity":9999
}